# tgv-functions
This package contains functions that are used often in producers for
projects. Installation instructions and specifications about the
functions are listed below.

## Installation
The package can be installed by providing the gitlab link to a pip
requirements file like below. 
```
pip3 install tgvfunctions@git+https://gitlab.com/the-green-village/tgv-functions.git
pip3 install tgvfunctions@git+https://gitlab.com/the-green-village/tgv-functions.git@commitnumber
pip3 install tgvfunctions@git+https://gitlab.com/the-green-village/tgv-functions.git@vX.Y.Z
```
The commit number can be obtained from the gitlab repo and can be used
to specify which commit you want to install. Alternatively, a specific verssion (e.g. v0.1.2) 
can be installed using the version tag. In case one just wants to
install the latest commit on the main branch, one can omit the commit 
number (see the first line). The only thing one still has to do then is
import the package by using:
```
from tgvfunctions import tgvfunctions
```

After which one can for example use

```
tgv = tgvfunctions.TGVFunction("name_topic")
producer = tgv.makeproducer("name_producer")
value = {"foo": 1, "bar": True, "spam": "test"}
tgv.produce(producer, value)
```

## Functions
As of 18-01-2023, the following functions exist:
### makeproducer():
This function makes a producer and automatically reads
the variables specified in the projectsecrets.py file.
To run it, the BOOTSTRAP_SERVERS, API_KEY, and API_PASSWORD
must be set.

### isotoepochms(dataframe, datacolumnname):
This function takes a dataframe and returns the dataframe
with an extra column with the timestampepoch in ms. To run
it, you must provide a dataframe and the name of the date
column containing an iso date. One could add more cases by 
specifying an extra parameter (e.g. format) in the future.

### produce(producer, value)
This function produces a value, after a producer has been
constructed with the makeproducer function listed above. 
To run it, one must provide the producer name, and the value
to be produced. 

### roundTime(Dt=None, roundTo=300, utc=False)
This function rounds a datetime to a number of seconds. The
default is 300 seconds (5 minutes) and uses the local time.
To use the utc time, one can specify utc=True. If no arguements
are passed to the function, the datetime used to round is the 
current time. For example, if it is currently 15:27, running
roundTime() would result in the time 15:25.
