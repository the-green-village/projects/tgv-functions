import requests
import datetime as dt
import os
import time
from typing import Callable, Any, Optional, Union
from confluent_kafka import Producer
from confluent_kafka.schema_registry import SchemaRegistryClient
from confluent_kafka.schema_registry.avro import AvroSerializer
from confluent_kafka.serialization import SerializationContext, MessageField


# Custom Exceptions
class CommunicationException(Exception):
    pass


class MissingRequiredKeyException(Exception):
    pass


class TGVFunctions:
    """
    A TGVFunctions object which handles creating producers as well as putting messages to kafka. Note that several enviroment variables
    relating to Kafka API information will need to be set in order for the instantiating of the object to happen succesfully
    """
    def __init__(self, topic: str):
        """
        Constructor for the TGVFunctions. Note that the following environment variables will
        need to be set in order to succesfully construct the object:

            - `KAFKA_TGV_API_KEY`
            - `KAFKA_TGV_API_PASSWORD`
            - `KAFKA_TGV_SCHEMA_REGISTRY_USERNAME`
            - `KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD`
            - `KAFKA_TGV_BOOTSTRAP_SERVERS` (Found in [the tgv documentation](https://gitlab.com/the-green-village/tgv-documentation#architecture-overview)
            - `KAFKA_TGV_SCHEMA_REGISTRY_URL` (Found in [the tgv documentation](https://gitlab.com/the-green-village/tgv-documentation#architecture-overview))

        Parameters
        ----------
        `topic: str`, Target topic for the producer to produce to
        """
        # Import Kafka secrets from environment
        self.api_key = os.environ['KAFKA_TGV_API_KEY']
        self.api_password = os.environ['KAFKA_TGV_API_PASSWORD']
        self.bootstrap_servers = os.environ['KAFKA_TGV_BOOTSTRAP_SERVERS']
        self.schema_registry_url = os.environ['KAFKA_TGV_SCHEMA_REGISTRY_URL']
        self.schema_registry_username = os.environ['KAFKA_TGV_SCHEMA_REGISTRY_USERNAME']
        self.schema_registry_password = os.environ['KAFKA_TGV_SCHEMA_REGISTRY_PASSWORD']
        self.topic = topic

        # Define headers, schema conf etc. Do not change variables here but change in projectsecrets.py. The projectsecrets.py file should be in the code folder.
        conf = {'url': self.schema_registry_url,
                'basic.auth.user.info': f"{self.schema_registry_username}:{self.schema_registry_password}"}
        schema_registry_client = SchemaRegistryClient(conf)
        r = requests.get(f"{self.schema_registry_url}/subjects/{self.topic}-value/versions/latest",
                         auth=(self.schema_registry_username, self.schema_registry_password))
        schema_str = r.json()['schema']

        conf = {'auto.register.schemas': False}
        self.avro_serializer = AvroSerializer(schema_registry_client,
                                              schema_str,
                                              conf=conf)
        self.ser_context = SerializationContext(self.topic, MessageField.VALUE)

    def make_producer(self, producer_name: str) -> Producer:
        """
        Producer factory, create an instance of a Producer object which can be used to produce messages

        Parameters
        ----------
        `producer_name: str`, the name for the producer to create a producer name is mandatory
        and should use the naming convention {topic}-producer.

        Returns
        -------
        `producer: Producer`, the Producer object which needs to be passed to `TGVFunctions.produce()` or `TGVFunctions.produce_fast()`
        """
        # Create the producer instance. For producername, typically use {project name}-producer
        producer = Producer({
            'client.id': producer_name,
            'bootstrap.servers': self.bootstrap_servers,
            'sasl.mechanisms': 'PLAIN',
            'security.protocol': 'SASL_SSL',
            'sasl.username': self.api_key,
            'sasl.password': self.api_password,
        })
        return producer

    def produce(self, producer: Producer, value: dict) -> None:
        """
        Produce a message and flush, explicitly waiting until succesfull delivery of the message is confirmed before continuing code execution.
        Flushing explicitly waits until it is confirmed that the message was delivered.

        Parameters
        ----------
        `producer: Producer`, producer object to use for producing the message
        `value: dict`, message to produce compliant with the schema as shown in [the documentation](https://gitlab.com/the-green-village/tgv-documentation#data-schema)

        Returns
        -------
        `None`
        """
        # Produce the actual value
        avro_value = self.avro_serializer(value, self.ser_context)
        producer.produce(topic=self.topic, value=avro_value)
        producer.poll(0)
        producer.flush()

    def produce_fast(self, producer, value):
        """
        Produce a message but don't flush. Note that
        if the application is terminated before eventual flushing
        was completed, then the message will not be pushed
        to kafka.

        Parameters
        ----------
        `producer: Producer`, producer object to use for producing the message
        `value: dict`, message to produced compliant with the schema as shown in [the documentation](https://gitlab.com/the-green-village/tgv-documentation#data-schema)

        Returns
        -------
        `None`
        """
        # Produce the actual value
        avro_value = self.avro_serializer(value, self.ser_context)
        producer.produce(topic=self.topic, value=avro_value)
        producer.poll(0)


def time_stamp_to_file(name: str, timestamp_now: int, catch_time_seconds: int = 3600):
    """
    Create a textfile called 'name.txt' in which the most recently produced timestamp (timestamp_now) is saved. If the file already exists,
    this script returns the value in the textfile. If the file did not exist, the script returns the current time minus `catch_time_seconds`

    Parameters
    ----------
    `name: str`, filename to write the timestamp to
    `timestamp_now: int`, timestamp in seconds since epoch
    `catch_time_seconds: int = 3600 `, fallback time-delta in case the file does not exist

    Returns
    -------
    `begintime: float`, POSIX timestamp (seconds since epoch)
    """
    if os.path.isfile(f"./{name}.txt"):
        # If {name}.txt exists, gather data from this text file until the most recent measurement
        with open(f"{name}.txt", "r") as fh:
            begintime = int(fh.read())
    else:
        # If {name}.txt doesn't exist, gather data from the last 'catch_time_seconds' seconds
        begintime = round_time(round_to=1) - dt.timedelta(seconds=catch_time_seconds)
        begintime = int(begintime.timestamp())

    # Save the most recent timestamp to text file
    with open(f"{name}.txt", "w") as fh:
        fh.write(str(timestamp_now))

    return begintime


def time_stamp_read(name: str, catchtimeseconds: int = 3600):
    """
    Read timestamp from a textfile called 'name.txt' in which the most recently produced timestamp is saved.
    If the file did not yet exist, the script creates the file, and saves and returns the current time minus catchtimeseconds

    Parameters
    ----------
    `name: str`, filename to write the timestamp to
    `catch_time_seconds: int = 3600 `, fallback time-delta in case the file does not exist

    Returns
    -------
    `begintime: float`, POSIX timestamp (seconds since epoch)
    """
    if os.path.isfile("./{name}.txt"):
        # If {name}.txt exists, gather data from this text file until the most recent measurement
        with open(f"{name}.txt", "r") as timefile:
            begintime = int(timefile.read())
    else:
        # If 'name'.txt doesn't exist, gather data from the last 'catchtimeseconds' seconds
        begintime = round_time(round_to=1) - dt.timedelta(seconds=catchtimeseconds)
        begintime = int(begintime.timestamp())

        # Save begintime to text file
        with open(f"{name}.txt", 'w') as timefile:
            timefile.write(str(begintime))
    return begintime


def iso_to_epoch_ms(iso_date: str) -> int:
    """
    Take an ISO 8601 formatted datetime string and convert it to a milliseconds since epoch timestamp
    Note that this function also properly converts strings formatted with the Z post-fix. This automatically gets replaced with +00:00

    The old version of this function used a pandas.DataFrame object instead, see the examples
    subsection on how to apply this function over a whole dataframe column

    Parameters
    ----------
    `iso_date: str`, the date to convert to a milliseconds since Epoch count

    Returns
    -------
    `ms: int`, milliseconds since epoch

    Exceptions
    ----------
    `TypeError`, thrown if `iso_date` is not of type `str`
    `ValueError`, thrown if `iso_date` is not a valid ISO 8601 string

    Examples
    --------
    The Python builtin function `map` can be used to transform a list of ISO 8601 datetime
    strings into epochms timestamps as shown in the example below
    ```Python
    >>> time_data = ["2024-06-14T06:01:51Z", "2024-06-14T06:02:58Z", "2024-06-14T06:03:37Z"]
    >>> time_data_epochms = list(map(tgvfunctions.iso_to_epoch_ms, time_data))
    >>> time_data_epochms
    [1718344911000, 1718344978000, 1718345017000]
    ```

    Suppose that we have a `pandas.DataFrame` object that contains the data. It is possible
    to use the `DataFrame.apply()` method to either append a new mutated copy of the row or we can directly
    mutate the column instead.

    We first create a new fake example dataframe
    ```Python
    >>> data = [10, 11, 10]
    >>> time_data = ["2024-06-14T06:01:51Z", "2024-06-14T06:02:58Z", "2024-06-14T06:03:37Z"]
    >>> df = pd.DataFrame({"data": data, "date": time_data})
    >>> df
       data             time_data
    0    10  2024-06-14T06:01:51Z
    1    11  2024-06-14T06:02:58Z
    2    10  2024-06-14T06:03:37Z
    ```

    - Option 1: Append a new column

    ```Python
    >>> df["epochms"] = df["date"].apply(tgvfunctions.iso_to_epoch_ms)
    >>> df
       data             time_data        epochms
    0    10  2024-06-14T06:01:51Z  1718344911000
    1    11  2024-06-14T06:02:58Z  1718344978000
    2    10  2024-06-14T06:03:37Z  1718345017000
    ```
    - Option 2: Mutate the column in place

    ```Python
    >>> df["time_data"] = df["time_data"].apply(tgvfunctions.iso_to_epoch_ms)
    >>> df
       data      time_data
    0    10  1718344911000
    1    11  1718344978000
    2    10  1718345017000
    ```
    """
    seconds = dt.datetime.fromisoformat(iso_date.replace("Z", "+00:00")).timestamp()
    ms = int(seconds * 1000)
    return ms


def round_time(timestamp: Optional[dt.datetime] = None, round_to: int = 300, utc: bool = True):
    """
    Round a datetime object down to the nearest multiple of `round_to`

    Parameters
    ----------
    `dt : datetime | None = None`, timestamp to round. Defaults to None which is interpreted as now.
    `round_to: int = 300` : Closest number of seconds to round to, defaults to 5 minutes.
    `utc: bool = True`, whether to use UTC time or something else, defaults to True.

    Returns
    -------
    `rounded_timestamp: datetime`, the timestamp rounded to the nearest multiple of `round_to`.

    Example
    -------
    if it is now 11:27, t = roundTime() will result in t = 11:25 (in a datetime format)
    """
    if timestamp is None:
        if not utc:
            timestamp = dt.datetime.now()
        else:
            timestamp = dt.datetime.now(dt.UTC)
    seconds = (timestamp.replace(tzinfo=None) - timestamp.min).seconds
    rounding = (seconds) // round_to * round_to

    return timestamp + dt.timedelta(0, rounding - seconds, -timestamp.microsecond)


def generate_kafka_message(project_id: str,
                           application_id: str,
                           device_id: str,
                           timestamp: int,
                           measurements: Union[dict, list],
                           project_description: Optional[str] = None,
                           application_description: Optional[str] = None,
                           device_description: Optional[str] = None,
                           device_manufacturer: Optional[str] = None,
                           device_type: Optional[str] = None,
                           device_serial: Optional[str] = None,
                           location_id: Optional[str] = None,
                           location_description: Optional[str] = None,
                           latitude: Optional[str] = None,
                           longitude: Optional[str] = None,
                           altitude: Optional[str] = None,) -> dict:
    """
    Thin wrapper to generate a python dict object containing a message that can be pushed to Kafka using the serializing producer.

    Parameters
    ----------
    Required Arguments:
        `project_id: str`, Unique name identifying the project
        `application_id: str`, Unique name identifying a sub-system within the project. If not applicable, make same as `device_id`
        `device_id: str`, Unique name identifying the measurement device
        `timestamp: int`, Timestamp in milliseconds since epoch (UTC timezone) corresponding to when the measurement was taken
        `measurements: dict | list[dict]`, dictionary or list of dictionaries of all the measurements corresponding to the above identifiers.
        Measurement dictionaries may be generated via the `tgvfunctions.generate_measurement_dict()` function or manually with the required
        field `measurement_id` and optional fields `unit`, `value` and `measurement_description`.

    See the documentation at [The Green Village](https://gitlab.com/the-green-village/tgv-documentation#data-schema) for a detailed description of what these field represent
    Optional Arguments (all default to `None`):
        `project_description: None | str`
        `application_description: None | str`
        `device_description: None | str`
        `device_manufacturer: None | str`
        `device_type: None | str`
        `device_serial: None | str`
        `location_id: None | str`
        `location_description: None | str`
        `latitude: None | str`
        `longitude: None | str`
        `altitude: None | str`

    Returns
    -------
    `dict`, dictionary compliant with the kafka schema containing the keys:

            - "project_id"
            - "application_id"
            - "device_id"
            - "timestamp"
            - "project_description"
            - "application_description"
            - "device_description"
            - "device_manufacturer"
            - "device_type"
            - "device_serial"
            - "location_id"
            - "location_description"
            - "latitude"
            - "longitude"
            - "altitude"
            - "measurements"


    Exceptions
    ----------
    `MissingRequiredKeyException`, gets thrown when a measurement without a measurement id is passed.
    """
    # Check if all measurements have an id, if not throw an exception
    for (idx, m) in enumerate(measurements):
        if "measurement_id" not in m.keys():
            raise MissingRequiredKeyException(f"Expected measurement_id key for measurement {idx}")

    # Wrap in list in case a signle measurement was passed
    if (isinstance(measurements, dict)):
        measurements = [measurements]

    return {"project_id": project_id,
            "application_id": application_id,
            "device_id": device_id,
            "timestamp": timestamp,
            "project_description": project_description,
            "application_description": application_description,
            "device_description": device_description,
            "device_manufacturer": device_manufacturer,
            "device_type": device_type,
            "device_serial": device_serial,
            "location_id": location_id,
            "location_description": location_description,
            "latitude": latitude,
            "longitude": longitude,
            "altitude": altitude,
            "measurements": measurements, }


def generate_measurement_dict(measurement_id: str,
                              value: Union[int, float, bool, str, None] = None,
                              unit: Optional[str] = None,
                              measurement_description: Optional[str] = None,) -> dict:
    """
    Utility function to quickly generate measurement dictionaries.
    Structure for measurements is as follows:

        - `measurement_id` (Required)
        - `unit`
        - `value`
        - `measurement_description`

    Parameters
    ----------
    `measurement_id: str`, descriptive name for the measurement
    `value: int | float | bool | str | None`, the measurement data. May be integer, float, boolean, string or None.
    `unit: str | None`, Unit associated with the measurement
    `measurement_description: str | None = None`, Optional more elaborate description of the measurement context if needed

    Returns
    -------
    `dict`, dictionary with keys "measurement_id", "measurement_description", "unit" and "value" containing the measurement
    """
    return {"measurement_id": measurement_id,
            "measurement_description": measurement_description,
            "unit": unit,
            "value": value, }


def retry(func: Callable) -> Callable:
    """
    Decorator for retrying a function when it raises an exception. The main purpose of this is to elegantly retry when an API request fails.
    By default, the decorator retries 3 times with a delay of 1 second after failing before finally raising an exception

    Params
    ------
    `func: Callable` The function that needs to be retried

    Exceptions
    ----------
    `Exception`, raises an exception if the function failed after 3 tries.

    Example
    -------
    In the following example, the function will be called a maximum of 3 times in case that an exception is raised within the body of `foo()`

    ```python
    @retry
    def foo():
        # Some function or routine that may raise an exception
    ```
    """
    def inner(*args, **kwargs):
        retries = 3
        delay = 1
        result = None
        for attempt in range(retries):
            try:
                result = func(*args, **kwargs)
                break
            # Generic Exception becuase we do not know a priori what the function might throw
            except Exception:
                if attempt >= retries:
                    raise CommunicationException(f"Function {func.__name__} failed after {retries} attempts")
                time.sleep(delay)
        return result
    return inner
